import json
import os
import shutil
import asyncio
import time
from threading import Thread
from typing import List

import pymongo

from app.api.virus_total import VirusTotalService
from app.auth.token_wrapper import key_required
from app.dao import GridFSFile
from app.utils.fs_utils import GridFSUtils
from flask import Blueprint, jsonify, request, send_from_directory
from flask_cors import cross_origin

from app.utils.crawler import AIOCrawler, CrawlingSession
from app import mongo_fs, app

api = Blueprint('api', __name__)


class SharedData:
    def __init__(self, files: List[GridFSFile]):
        self.files = files
        self.results = len(files) * [0]


def save_and_clear(index, shared_data: SharedData, scan=False):
    file = shared_data.files[index]

    if scan:
        file.percentage = VirusTotalService().scan_file(file.path)
        try:
            file.malware = file.percentage >= 50
        except TypeError:
            print('Unsupported comparison, zipfile probably contains folders')

    shared_data.results[index] = str(mongo_fs.insert_file(file))


@api.route('/findall', methods=['GET'])
@key_required
def find_all():
    return jsonify(mongo_fs.find_all()), 200


@api.route('/getcsv', methods=['GET'])
@key_required
def get_data():
    return mongo_fs.make_csv(), 200


@api.route('/getmalware', methods=['GET'])
@key_required
def get_malware():
    return mongo_fs.get_viruses(), 200


@api.route('/createzip', methods=['GET', 'POST'])
@key_required
def create_zip():
    file_ids = request.values.get('ids').split(',')
    zipfile = mongo_fs.return_zip(file_ids)
    filepath = (os.path.join(app.root_path, app.config['UPLOAD_FOLDER']))

    def clear(filepath, zipfile):
        time.sleep(10)
        os.remove(filepath + '/' + zipfile)

    t = Thread(group=None, target=clear, args=(filepath, zipfile))
    t.start()

    return send_from_directory(filepath, zipfile, as_attachment=True), 200


@api.route('/upload', methods=['POST'])
@cross_origin(supports_credentials=True)
@key_required
def upload_zip():
    try:
        scan = request.args.get('scan') == 'true'
    except KeyError:
        return 'Missing parameter `scan:bool`', 400
    folder_path, zip_path = GridFSUtils.extract_binary_zip(os.path.join(f'app/tmp/'), request.files.get('file'))
    shared_data = SharedData(files=GridFSUtils.to_grid_fs(folder_path))
    threads: List[Thread] = []

    for i in range(len(shared_data.files)):
        threads.append(
            Thread(group=None, target=save_and_clear, args=(i, shared_data, scan))
        )

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    shutil.rmtree(folder_path)
    os.remove(zip_path)

    return jsonify(shared_data.results), 200


@api.route('/crawl', methods=['POST'])
@key_required
def crawl():
    root_url = json.loads(request.data.decode())['root']
    limit = int(request.args.get('max'))

    if not root_url:
        return 'Missing root url', 400

    user = mongo_fs.get_user_by_apikey(request.args.get('key'))
    crawler = AIOCrawler(root_url, mongo_fs, CrawlingSession.for_user(user['username']), maximum=limit)
    asyncio.run(crawler.crawl())
    mongo_fs.clear_running_crawl_logs_for_user(user['username'])

    return '', 200


@api.route('/crawled', methods=['GET'])
@key_required
def get_crawl_logs_for_user():
    username = request.args.get('for')

    crawled_logs = [
        {'message': log['message'], 'session': log['session'], 'started_at': log['started_at'], 'root': log['root']}
        for log in mongo_fs.get_crawled(username)
    ]
    result = {}

    for log in crawled_logs:
        if log['session'] not in result:
            content = {'root': log['root'], 'started_at': log['started_at'], 'session': log['session'], 'logs': []}
            result[log['session']] = content
        result[log['session']]['logs'].append(log['message'])

    return jsonify(list(result.values())), 200


@api.route('/getdata', methods=['POST'])
@key_required
def getData():
    try:
        filter = request.form.get('filterBy')
    except KeyError:
        return 'Missing parameter `filterBy:"name"`', 400
    try:
        order = request.form.get('order')
    except KeyError:
        return 'Missing parameter `asc:bool`', 400

    if str(order).lower() == 'asc':
        order = pymongo.ASCENDING
    else:
        order = pymongo.DESCENDING

    return mongo_fs.find_file_by_param(str(filter).lower(), order)

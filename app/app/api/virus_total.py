import os
from datetime import datetime
from time import sleep

import requests
from bson import ObjectId

from app import mongo


class VirusTotalService:
    def __init__(self):
        self.SMALL_FILE_THRESHOLD = 33554432
        self.API_KEY = '2074666fef6338f2f1d18c9bd84f060b222a9d4fc71f7b4f546425ac040870ed'
        self._default_headers = {'x-apikey': self.API_KEY}
        self._BASE_URL = 'https://www.virustotal.com/api/v3'

    def upload_small_file(self, file):
        data = {'file': open(file, 'rb')}
        response = requests.post(f'{self._BASE_URL}/files', headers=self._default_headers, files=data)
        url = f"{self._BASE_URL}/analyses/{response.json()['data']['id']}"
        response = requests.get(url, headers=self._default_headers)

        while response.json()['data']['attributes']['status'] != 'completed':
            sleep(15)
            url = f"{self._BASE_URL}/analyses/{response.json()['data']['id']}"
            response = requests.get(url, headers=self._default_headers)

        return self.process_analysis(response)

    def upload_large_file(self, file):
        with open(file, 'rb') as binary:
            data = {'file': binary}
            response = requests.get(f'{self._BASE_URL}/files/upload_url', headers=self._default_headers)
            response = requests.post(response.json()['data'], headers=self._default_headers, files=data)
        
        while response.json()['data']['attributes']['status'] != 'completed':
            sleep(15)
            url = f"{self._BASE_URL}/analyses/{response.json()['data']['id']}"
            response = requests.get(url, headers=self._default_headers)   

        return self.process_analysis(response)

    def scan_file(self, input_file):
        file_size = os.stat(input_file).st_size
        scan_result = None
        _id = mongo.db.running_uploads.insert(
            {
                'filename': input_file.split('/')[-1],
                'started': datetime.now()
            }
        )

        try:
            if file_size < self.SMALL_FILE_THRESHOLD:
                scan_result = self.upload_small_file(input_file)
            else:
                scan_result = self.upload_large_file(input_file)
            print('------------------------------------------------', scan_result)
        except Exception:
            # safety first
            pass
        finally:
            mongo.db.running_uploads.delete_one({'_id': ObjectId(_id)})

        return scan_result

    def process_analysis(self, response):
        attr = response.json()['data']['attributes']
        sum_all = len(attr['results'])
        stats = attr['stats']
        sum_unsuccessful = stats['type-unsupported'] + stats['confirmed-timeout'] + stats['timeout'] + stats[
            'failure']
        sum_mal = stats['malicious'] + stats['suspicious']

        if sum_all == sum_unsuccessful:
            return -1
        else:
            return round(((sum_all - sum_unsuccessful) / 100) * sum_mal, 2)

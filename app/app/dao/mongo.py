import csv
import json
import logging
import os
import zipfile
from dataclasses import dataclass
from datetime import datetime
from typing import Collection, List

from bson import ObjectId
from flask import Flask
from flask_pymongo import PyMongo
from gridfs import GridFS

logger = logging.getLogger(__name__)


@dataclass()
class GridFSFile:
    path: str
    percentage: float = 0
    malware: bool = False
    crawling_session: str = None

    def __str__(self) -> str:
        return f'GridFSFile: {{filename: {self.path}, malware: {self.malware}, percentage: {self.percentage}}}'


@dataclass(frozen=True)
class MongoCredentials:
    username: str = None
    password: str = None
    host: str = None
    db_name: str = None

    @property
    def uri(self):
        return f'mongodb://{self.username}:{self.password}@{self.host}:27017/{self.db_name}'


class MongoFS:
    _credentials: MongoCredentials = None
    _mongo: PyMongo = None
    _grid_fs: GridFS = None

    def __init__(self, username: str, password: str, host: str, db_name: str, app: Flask) -> None:
        self._credentials = MongoCredentials(username, password, host, db_name)
        self._mongo = PyMongo(app=app, uri=self._credentials.uri)
        self._mongo_fs = GridFS(self._mongo.db)

    def get_uri(self):
        return self._credentials.uri

    @classmethod
    def from_env(cls, app: Flask):
        return MongoFS(
            os.environ['MONGODB_USERNAME'],
            os.environ['MONGODB_PASSWORD'],
            os.environ['MONGODB_HOSTNAME'],
            os.environ['MONGODB_DATABASE'],
            app=app
        )

    def insert_file(self, grid_file: GridFSFile) -> ObjectId:
        with open(grid_file.path, 'rb') as file:
            logger.info(f'Saving {grid_file.path}')
            return self._mongo_fs.put(
                file,
                filename=grid_file.path.split('/')[-1],
                malware=grid_file.malware,
                percentage=grid_file.percentage,
                crawling_session=grid_file.crawling_session
            )

    def insert_bulk(self, grid_files: Collection[GridFSFile]) -> List[ObjectId]:
        _ids: List[ObjectId] = []

        for grid_file in grid_files:
            _ids.append(self.insert_file(grid_file))

        return _ids

    def find_all(self):
        return [{'id': str(item._id), 'filename': item.filename, 'length': item.length, 'percentage': item.percentage} for
                item in self._mongo_fs.find()]

    def find_single_file(self, file_id):
        return [{'id': str(item.file_id), 'filename': item.filename, 'length': item.length, 'malware': item.malware} for
                item in self._mongo_fs.find({'_id': ObjectId(id)})]

    def get_path(self):
        path = os.getcwd()
        str2 = path.split('\\')
        separator = '/'
        return separator.join(str2) + '/app/tmp/'

    def make_csv(self):
        db_object = self.find_all()
        csv_name = 'database_export.csv'
        with open(f'{self.get_path()}{csv_name}', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['_id', 'file_name', 'length', 'malware'])
            for item in db_object:
                array_object = []
                for i in item:
                    array_object.append(item.get(i))
                writer.writerow(array_object)
        return f"{self.get_path()}{csv_name}"

    def return_zip(self, array):
        ct = datetime.now().timestamp()
        z = zipfile.ZipFile(f'{self.get_path()}{ct}.zip', 'w')

        for id in array:
            file = self._mongo_fs.find_one(dict(_id=ObjectId(id)))
            data = file.read()
            with open(f'{self.get_path()}{file.filename}', 'wb') as out:
                out.write(data)
            z.write(f"{self.get_path()}{file.filename}", file.filename)
            os.remove(f"{self.get_path()}{file.filename}")
        z.close()
        return f'{ct}.zip'

    def get_viruses(self):
        _zip = zipfile.ZipFile(f"{self.get_path()}virus.zip", "a")
        data = self.find_all()
        for obj in data:
            # tuto podmienku este bude treba zmenit ak sa teda bude menit vyhodnocovanie a jeho zapis do db
            if obj.get('malware'):
                file = self._mongo_fs.find_one({'_id': ObjectId(obj.get('_id'))})
                data = file.read()
                out = open(f'{self.get_path()}{file.filename}', "wb")
                out.write(data)
                out.close()
                _zip.write(f'{self.get_path()}{file.filename}', file.filename)
                _zip.close()
                os.remove(f'{self.get_path()}{file.filename}')
            else:
                print('No malware')
        return 'Test'

    def get_user_by_apikey(self, apikey):
        return self._mongo.db.auth.find_one({'apiKey': apikey})

    def get_crawled(self, username):
        return self._mongo.db.crawl_logs.find({'username': username})

    def clear_running_crawl_logs_for_user(self, username):
        self._mongo.db.running_crawls.remove({'username': username})
        
    def find_file_by_param(self, filter, order):
        useless_data = ['_id', 'chunkSize']
        files = self._mongo_fs.find().sort(filter, order)
        
        out = [x._file for x in files]
        for item in out:
            for data in useless_data:
                item.pop(data)
            item['uploadDate'] = str(item['uploadDate'])

        return json.dumps(out)

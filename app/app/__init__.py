import logging
import os

from flask import Flask, render_template
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_login import LoginManager
from flask_pymongo import PyMongo
from flask_socketio import SocketIO

from .dao import MongoFS
from .helpers.notes_builder import build_notes


app: Flask = Flask(__name__, template_folder='../templates', static_folder='../static')
mongo_fs: MongoFS = MongoFS.from_env(app)
app.config['MONGO_URI'] = mongo_fs.get_uri()
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = '../tmp'
mongo = PyMongo()
b_crypt = Bcrypt()
login_manager = LoginManager()
socket_io = SocketIO()

logging.basicConfig(level=logging.DEBUG)


def create_app():
    CORS(app, resources={r'/*': {'origins': '*'}})
    app.secret_key = os.environ['SECRET_KEY']
    mongo.init_app(app)
    b_crypt.init_app(app)
    login_manager.init_app(app)
    socket_io.init_app(app, cors_allowed_origins='*')

    from .auth.routes import auth
    from .api.routes import api

    app.register_blueprint(auth, url_prefix='/api')
    app.register_blueprint(api, url_prefix='/api')

    return app


@socket_io.on('get_running_tasks')
def get_running_tasks():
    uploads = [{'filename': item['filename'], 'started': str(item['started'])} for item in
               mongo.db.running_uploads.find()]

    socket_io.send({'uploads': uploads})


@app.route('/')
def hello_world():
    app.logger.info('Hello World!')
    return 'Hello World!'


@socket_io.on('get_running_crawl_for_user')
def get_running_crawl_for_user(data):
    crawls = mongo.db.running_crawls.find({'username': data})

    socket_io.send({'running_crawls': crawls})


@app.route('/team')
def about():
    notes = build_notes()
    return render_template('index.jinja2', zapisnice=notes)

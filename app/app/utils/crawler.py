import json
import os
import uuid
from datetime import datetime

import aiohttp
from aiohttp import ClientConnectorError
from bs4 import BeautifulSoup

from dao import GridFSFile


class CrawlingSession:
    def __init__(self, started_by):
        self.id = uuid.uuid4().hex
        self.started_by = started_by

    @classmethod
    def for_user(cls, username):
        return CrawlingSession(username)


class AIOCrawler:
    def __init__(self, root: str, mongo_fs, session: CrawlingSession, maximum: int = 5):
        self.root = root
        self.urls_to_crawl = [root]
        self.already_crawled = []
        self.files_downloaded = 0
        self.maximum = maximum
        self.session: CrawlingSession = session
        self.mongo_fs = mongo_fs
        self.started_at = datetime.now()
        self.headers = {  # google hack headers
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        }
        with open('app/app/utils/crawler-conf.json', 'r') as json_conf:
            self.config = json.load(json_conf)

    async def _request_headers(self, url: str):
        async with aiohttp.ClientSession() as session:
            try:
                async with session.head(url) as response:
                    return response.headers
            except ClientConnectorError:
                log = {
                    'message': f'[SKIP]-{url}',
                    'session': self.session.id,
                    'username': self.session.started_by,
                    'started_at': self.started_at,
                    'root': self.root
                }
                self.mongo_fs._mongo.db.running_crawls.insert(log)
                self.mongo_fs._mongo.db.crawl_logs.insert(log)
                # print(f'[SKIP]\t-\t{url}')
                pass

    async def _request_body(self, url: str):
        async with aiohttp.ClientSession(headers=self.headers) as session:
            try:
                async with session.get(url, allow_redirects=False) as response:
                    return await response.text()
            except Exception:
                # print(f'[SKIP]\t-\t{url}')
                log = {
                    'message': f'[SKIP]-{url}',
                    'session': self.session.id,
                    'username': self.session.started_by,
                    'started_at': self.started_at,
                    'root': self.root
                }
                self.mongo_fs._mongo.db.running_crawls.insert(log)
                self.mongo_fs._mongo.db.crawl_logs.insert(log)
                return None

    async def _extract_hrefs(self, links, main_url):
        hrefs = []

        for link in links:
            if link['href'].startswith('#'):  # is section
                continue
            if link['href'].startswith('/'):  # subpage
                link['href'] = main_url + link['href'][1:]  # remove first '/'
            if not link['href'].startswith('http'):
                continue
            if link['href'].startswith('www'):
                link['href'] = 'http://' + link['href']
            if link['href'].count(':') > 1:
                continue
            if '#' in link['href']:
                link['href'] = ''.join(link['href'].split('#')[:-1])
            hrefs.append(link['href'])

        return list(set(hrefs))  # drop duplicates

    async def scan_uris(self, url: str):
        html = await self._request_body(url)
        if html:
            return await self._extract_hrefs(BeautifulSoup(html, 'html.parser').find_all('a', href=True), url)
        return []

    async def download_executable(self, url):
        try:
            file_name = url.replace('https://', '').replace('http://', '').split('/')[-1]

            if file_name != '':
                file_extension = file_name.split('.')[-1]
            else:
                return False

            headers = await self._request_headers(url)
            content_type = headers['Content-Type'].split(';')[0]

            if (file_extension is not None) and ((content_type in self.config['mimetypes-exes']) or
                                                 (file_extension.upper() in self.config['file-extensions'])):
                body = await self._request_body(url)
                encoding = headers['Content-Type'].split('=')[-1] if '=' in headers['Content-Type'] else 'UTF-8'

                with open('app/tmp/' + file_name, 'wb+') as executable:
                    # print(f'[DOWNLOADING]\t-\t{file_name} from {url}')
                    log = {
                        'message': f'[DOWNLOADING]-{file_name} from {url}',
                        'session': self.session.id,
                        'username': self.session.started_by,
                        'started_at': self.started_at,
                        'root': self.root
                    }
                    self.mongo_fs._mongo.db.running_crawls.insert(log)
                    self.mongo_fs._mongo.db.crawl_logs.insert(log)
                    executable.write(bytearray(body, encoding=encoding))

                grid_file = GridFSFile('app/tmp/' + file_name, None, None, self.session.id)
                self.mongo_fs.insert_file(grid_file)
                os.remove(grid_file.path)

                return True
        except Exception as e:
            print(e)
            return False

    async def crawl(self):
        for url in self.urls_to_crawl:
            if self.files_downloaded >= self.maximum:
                log = {
                    'message': f'FINISHED',
                    'session': self.session.id,
                    'username': self.session.started_by,
                    'started_at': self.started_at,
                    'root': self.root
                }
                print(log)
                self.mongo_fs._mongo.db.running_crawls.insert(log)
                self.mongo_fs._mongo.db.crawl_logs.insert(log)
                break

            # print(f'[CRAWLING]\t-\t{url}')
            log = {
                'message': f'[CRAWLING]-{url}',
                'session': self.session.id,
                'username': self.session.started_by,
                'started_at': self.started_at,
                'root': self.root
            }
            print(log)
            self.mongo_fs._mongo.db.running_crawls.insert(log)
            self.mongo_fs._mongo.db.crawl_logs.insert(log)

            if url in self.already_crawled:
                self.urls_to_crawl.remove(url)
                continue

            result = await self.download_executable(url)

            if result:
                self.files_downloaded += 1

            self.urls_to_crawl += await self.scan_uris(url)
            self.urls_to_crawl.remove(url)
            self.already_crawled.append(url)

import os
import uuid
import zipfile

from app.dao import GridFSFile


class GridFSUtils:
    @staticmethod
    def extract_binary_zip(path, binary_zip, create_dir=True):
        _id: uuid.UUID = uuid.uuid1()

        if create_dir and not os.path.isdir(path):
            os.mkdir(path)

        folder_path: str = os.path.join(f'{path}{_id}')
        zip_path: str = os.path.join(f'{path}{_id}.zip')

        with open(zip_path, 'wb') as zip_file:
            zip_file.write(binary_zip.read())

        with zipfile.ZipFile(zip_path, 'r') as saved_zip:
            saved_zip.extractall(folder_path)

        return folder_path, zip_path

    @staticmethod
    def to_grid_fs(folder_path):
        grid_files = []

        for file in os.listdir(folder_path):
            grid_files.append(GridFSFile(f'{folder_path}/{file}'))

        return grid_files

from flask_login import UserMixin
from app import app, mongo


class User(UserMixin):

    def __init__(self, username, password, email, apiKey):
        self.username = username
        self.password = password
        self.email = email
        self.apiKey = apiKey

    def get_id(self):
        return self.username


@app.login_manager.user_loader
def load_user(user_id):
    found = mongo.db.auth.find_one({'username': user_id})
    return User(found['username'], found['password'], found['email'], found['apiKey'])

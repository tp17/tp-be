import os
from binascii import hexlify

from app import b_crypt, mongo
from flask import Blueprint, request, jsonify, g
from flask_login import current_user, login_user, logout_user
from .user_model import User

auth = Blueprint('auth', __name__)


@auth.route('/register', methods=['POST'])
def register_user():
    if current_user and current_user.is_authenticated:
        return jsonify({'success': False,
                        'message': 'Already logged in'}), 200

    form = request.form

    if mongo.db.auth.find_one({'email': form['email']}):
        return jsonify({'success': False,
                        'message': 'Email already in use'}), 200

    if mongo.db.auth.find_one({'username': form['username']}):
        return jsonify({'success': False,
                        'message': 'Username already in use'}), 200

    key = hexlify(os.urandom(16))
    user = {
        'username': form['username'],
        'email': form['email'],
        'password': b_crypt.generate_password_hash(form['password']).decode('utf-8'),
        'apiKey': key.decode('utf-8')
    }
    mongo.db.auth.save(user)

    return jsonify({'success': True,
                    'message': 'Account created'}), 200


@auth.route('/login', methods=['POST'])
def login():
    if current_user.is_authenticated:
        return jsonify({'success': False,
                        'message': 'Already logged in'}), 200

    form = request.form
    result = mongo.db.auth.find_one({'username': form['username']})
    if result and b_crypt.check_password_hash(result['password'], form['password']):
        user = User(result['username'], result['password'], result['email'], result['apiKey'])
        login_user(user)
        return jsonify({
            'success': True,
            'message': 'Login successful',
            'user': {'username': result['username'],
                     'email': result['email'],
                     'apiKey': result['apiKey']}
        }), 200
    else:
        return jsonify({'success': False,
                        'message': 'Bad password'}), 200


@auth.route('/logout', methods=['POST'])
def logout():
    logout_user()
    return jsonify({'success': True,
                    'message': 'Successfully logged out'}), 200
    
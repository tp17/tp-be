import datetime
import json
import os

from fpdf import FPDF


def build_notes():
    notes = []
    pdfs = []
    for filename in os.listdir('app/static/notes'):
        if filename.endswith('.json'):
            with open('app/static/notes/' + filename, encoding='utf8') as file:
                data = json.load(file)
                note_obj = {
                    'path': filename,
                    'date': filename.replace('-', '.').replace('.json', ''),
                    'title': data['title'],
                    'subtitle': data['subtitle'],
                    'present': data['present']
                }
                build_pdf(note_obj['path'], data['subtitle'], data['title'], data['text'], data['present'])
                pdf_obj = {
                    'path': filename.replace('.json', '.pdf'),
                    'date': filename.replace('-', '.').replace('.json', '')
                }
                notes.append(note_obj)
                pdfs.append(pdf_obj)
    return sorted(notes, key=lambda x: datetime.datetime.strptime(x['date'], '%d.%m.%Y'))


def build_pdf(name, sub, title, text, present):
    pdf = FPDF()
    pdf.add_page()

    pdf.add_font('DejaVu', '', 'app/static/font/DejaVuSansCondensed.ttf', uni=True)
    pdf.set_font('DejaVu', '', 15)
    pdf.cell(80)
    pdf.multi_cell(0, 5, title, 'C')
    pdf.ln()
    pdf.ln()
    pdf.ln()
    pdf.multi_cell(0, 5, f'Preberaná téma: {sub}')
    pdf.set_font('DejaVu', '', 12)
    pdf.ln()
    pdf.multi_cell(0, 5, f'Prítomní: {present}')
    pdf.ln()
    pdf.multi_cell(0, 5, text)
    pdf.ln()
    pdf.output(f'app/static/notes/{name.replace(".json", ".pdf")}', 'F')

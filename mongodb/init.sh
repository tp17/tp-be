#!/usr/bin/env bash

echo 'Creating application user and db'

mongo test_db \
        --host localhost \
        --port 27017 \
        -u admin \
        -p admin \
        --authenticationDatabase admin \
        --eval "db.createUser({user: 'root', pwd: 'root', roles:[{role:'dbOwner', db: 'test_db'}]});"

